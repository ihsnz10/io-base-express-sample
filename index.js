const express = require('express');
const path = require('path');

const app = express();
const port = process.env.PORT || 3000;

// sendFile will go here
app.get('/', function(req, res) {
  res.sendFile(path.join(__dirname, '/index.html'));
});

app.get('/health', (req, res) => {
  res.send('Hello World!')
})

app.get('/healthfail', (req, res) => {
  res.status(500).send('Something broke!')
})

app.listen(port);
console.log('Server started at http://localhost:' + port);